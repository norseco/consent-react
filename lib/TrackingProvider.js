"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.ConsentTrackerContext = void 0;

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var ConsentTrackerContext = _react.default.createContext();

exports.ConsentTrackerContext = ConsentTrackerContext;

var TrackingProvider = function TrackingProvider(_ref) {
  var tracker = _ref.tracker,
      children = _ref.children;
  return _react.default.createElement(_react.default.Fragment, null, _react.default.createElement(ConsentTrackerContext.Provider, {
    value: tracker
  }, children));
};

TrackingProvider.propTypes = {
  children: _propTypes.default.any,
  tracker: _propTypes.default.object.isRequired
};
var _default = TrackingProvider;
exports.default = _default;