import React from 'react';
import { withTracking } from '.';

const withConsent = Component => {
  class withConsent extends React.Component {

    constructor(props) {
      super(props);
      this.accept = this.accept.bind(this);
      this.decline = this.decline.bind(this);
    }

    accept() {
      this.props.tracker.setConsent(true);
      this.forceUpdate();
    }

    decline() {
      this.props.tracker.setConsent(false);
      this.forceUpdate();
    }

    render() {
      return (
        <React.Fragment>
          { !this.props.tracker.checkDecided() && this.props.tracker.checkRequiredConsent() && (
            <Component
              {...this.props}
              accept={this.accept}
              decline={this.decline}
            />
          )}
        </React.Fragment>
      )
    }
  }
  return withTracking(withConsent);
};

export default withConsent;
